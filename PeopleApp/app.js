Ext.application({
    requires: ['Ext.container.Viewport'],
    name: 'PeopleApp',
 
    appFolder: 'app',
    controllers: ['Peoples'],
     
    launch: function() {
        Ext.create('Ext.container.Viewport', {
            layout: 'fit',
            items: {
                xtype: 'peoples',
            }
        });
    }
});