Ext.define('PeopleApp.store.People', {
    extend: 'Ext.data.Store',
    model: 'PeopleApp.model.People',
    autoLoad: true,
    storeId: 'People',
    proxy: {
        type: 'ajax',
        url: 'app/data/peoples.json',
        reader: {
            type: 'json',
            rootProperty: 'peoples',
            successProperty: 'success'
        }
    }
});