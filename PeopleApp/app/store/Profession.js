Ext.define('PeopleApp.store.Profession', {
    extend: 'Ext.data.Store',
    model: 'PeopleApp.model.Profession',
    autoLoad: true,
    storeId: 'Profession',
    proxy: {
        type: 'ajax',
        url: 'app/data/professions.json',
        reader: {
            type: 'json',
            rootProperty: 'professions',
            successProperty: 'success'
        }
    }
});
