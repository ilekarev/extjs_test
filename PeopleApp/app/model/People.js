Ext.define('PeopleApp.model.People', {
    extend: 'Ext.data.Model',
    fields: [
        { name: 'id' },
        { name: 'name' },
        { name: 'email' },
        { name: 'birthday' },
        { name: 'professionId' },
    ],
});