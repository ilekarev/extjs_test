Ext.define('PeopleApp.model.Profession', {
    extend: 'Ext.data.Model',
    fields: ['name'],
});
