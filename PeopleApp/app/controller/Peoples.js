Ext.define('PeopleApp.controller.Peoples', {
    extend: 'Ext.app.Controller',
 
    views: ['Peoples', 'People'],
    stores: ['People', 'Profession'],
    models: ['People', 'Profession'],
    init: function() {
        this.control({
            'viewport > peoples': {
                itemdblclick: this.editPeople
            },
            'peoplewindow button[action=save]': {
                click: this.updatePeople
            },
        });
    },

    updatePeople: function(button) {
        var win = button.up('window');
        var form = win.down('form');
        var values = form.getValues();

        var birthdayData = values.birthday.split('.');
        values.birthday = birthdayData[1] + '/' + birthdayData[0] + '/' + birthdayData[2];

        form.getRecord().set(values);

        win.close();
    },

    editPeople: function(grid, record) {
        var view = Ext.widget('peoplewindow');
        view.down('form').loadRecord(record);
    }
});