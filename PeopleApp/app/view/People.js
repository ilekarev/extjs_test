Ext.define('PeopleApp.view.People', {
    extend: 'Ext.window.Window',
    alias: 'widget.peoplewindow',

    title: 'Сотрудник',
    layout: 'fit',
    autoShow: true,

    initComponent: function() {
        this.items = [
            {
                xtype: 'form',
                items: [
                    {
                        xtype: 'textfield',
                        name : 'name',
                        fieldLabel: 'Название',
                        minLength: 2,
                        maxLength: 100,
                        allowBlank: false,
                    },
                    {
                        xtype: 'textfield',
                        name : 'email',
                        fieldLabel: 'Email',
                        vtype: 'email',
                        allowBlank: false,
                    },
                    {
                        xtype: 'datefield',
                        name : 'birthday',
                        fieldLabel: 'Дата рождения',
                        format: 'd.m.Y',
                        vtype: 'birthday',
                        allowBlank: false,
                    },
                    {
                        xtype: 'combobox',
                        name: 'professionId',
                        fieldLabel: 'Профессия',
                        store: 'Profession',
                        displayField: 'name',
                        valueField: 'id',
                        allowBlank: false,
                        editable: false,
                    }
                ],
                listeners: {
                    validitychange: function(view, isValid) {
                        Ext.getCmp('submitButton').setDisabled(!isValid);
                    }
                }
            }
        ];
        this.buttons = [{
            id: 'submitButton',
            text: 'Сохранить',
            scope: this,
            action: 'save',
        }];
 
        this.callParent(arguments);
    }
});

Ext.define('Override.form.field.VTypes', {
    override: 'Ext.form.field.VTypes',

    birthday:  function(value) {
        return this.birthdayRe.test(value);
    },
    birthdayRe: /^\d{2}\.\d{2}\.\d{4}$/,
    birthdayText: 'Дата должна соответствовать формату ДД.ММ.ГГГГ',
    birthdayMask: /[\d\.]/i
});