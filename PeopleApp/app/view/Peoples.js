Ext.define('PeopleApp.view.Peoples' ,{
    extend: 'Ext.grid.Panel',
    alias: 'widget.peoples',

    title: 'Сотрудники',
    store: 'People',

    initComponent: function() {
        var grid = this;
        var professionStore = Ext.getStore('Profession');
        professionStore.on("load", function() {
            grid.getView().refresh();
        });

        this.columns = [
            {
                header: 'Имя',
                dataIndex: 'name',
                flex: 1,
            },
            {
                header: 'Email',
                dataIndex: 'email',
                flex: 1,
            },
            {
                header: 'Дата рождения',
                dataIndex: 'birthday',
                xtype: 'datecolumn',
                format: 'd.m.Y',
                flex: 1,
            },
            {
                header: 'Профессия',
                dataIndex: 'professionId',
                flex: 1,
                renderer: function(value, metaData, record ) {
                    var profession = professionStore.findRecord('id', value);
                    return profession != null ? profession.get('name') : value;
                }
            },
        ];

        this.callParent(arguments);
    },

    listeners: {
        rowcontextmenu: function (view, record, tr, rowIndex, e, eOpts) {
            e.stopEvent();
            Ext.Msg.show({
                title:'Удалить запись?',
                message: 'Вы уверены, что хотите удалить запись?',
                buttons: Ext.Msg.YESNO,
                buttonText: {
                    yes: 'Да',
                    no: 'Нет',
                },
                icon: Ext.Msg.QUESTION,
                fn: function(btn) {
                    if (btn === 'yes') {
                        var peoplesStore = Ext.widget('peoples').getStore();
                        peoplesStore.remove(record);
                    }
                }
            });
        }
    }

});
